package group.tothepoint.todo.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import group.tothepoint.todo.model.TodoItem;
import group.tothepoint.todo.service.TodoService;

@RunWith(MockitoJUnitRunner.class)
public class TodoControllerTest {
    @InjectMocks
    private TodoController controller;
    @Mock
    private TodoService service;

    @Test
    public void addTodoShouldReturnExpectedTodoItem() {
        String todo = RandomStringUtils.randomAlphabetic(13);
        TodoItem expected = TodoItem.builder()
                .withTodo(todo)
                .build();

        when(service.addTodo(todo)).thenReturn(expected);

        assertThat(controller.addTodo(todo)).isEqualToComparingFieldByField(expected);
    }

    @Test
    public void findAllShouldReturnExpectedList() {
        List<TodoItem> expected = Arrays.asList(
                TodoItem.builder()
                        .withTodo(RandomStringUtils.randomAlphabetic(13))
                        .build(),
                TodoItem.builder()
                        .withTodo(RandomStringUtils.randomAlphabetic(13))
                        .build()
        );

        when(service.findAll()).thenReturn(expected);

        assertThat(controller.findAll()).isEqualTo(expected);
    }
}
