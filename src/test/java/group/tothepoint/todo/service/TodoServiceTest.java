package group.tothepoint.todo.service;


import static org.assertj.core.api.Assertions.assertThat;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;

import group.tothepoint.todo.model.TodoItem;

public class TodoServiceTest {

    private TodoService service;

    public TodoServiceTest() {
        service = new TodoService();
    }

    @Test
    public void addTodo() {
        String todo = RandomStringUtils.randomAlphabetic(24);

        TodoItem item = service.addTodo(todo);

        assertThat(item.getId()).isNotEmpty();
        assertThat(item.getTodo()).isEqualTo(todo);

        assertThat(service.items).hasSize(1);
    }
}