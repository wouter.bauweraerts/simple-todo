package group.tothepoint.todo.model;

import java.util.Objects;
import java.util.UUID;

public class TodoItem {
    private String id;
    private String todo;

    public TodoItem(final String id, final String todo) {
        this.id = id;
        this.todo = todo;
    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final TodoItem todoItem = (TodoItem) o;
        return Objects.equals(id, todoItem.id) &&
                Objects.equals(todo, todoItem.todo);
    }

    public String getId() {
        return id;
    }

    public String getTodo() {
        return todo;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, todo);
    }

    public static class Builder {
        private String todo;

        public Builder withTodo (String todo) {
            this.todo = todo;
            return this;
        }

        public TodoItem build() {
            return new TodoItem(UUID.randomUUID().toString(), todo);
        }
    }
}
