package group.tothepoint.todo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import group.tothepoint.todo.model.TodoItem;

@Service
public class TodoService {
    protected List<TodoItem> items;

    public TodoService() {
        this.items = new ArrayList<>();
    }

    public TodoItem addTodo(final String todo) {
        TodoItem newItem = TodoItem.builder().withTodo(todo).build();
        items.add(newItem);
        return newItem;
    }

    public List<TodoItem> findAll() {
        return new ArrayList<>(items);
    }
}
