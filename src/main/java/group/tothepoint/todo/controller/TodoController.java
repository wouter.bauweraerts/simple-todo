package group.tothepoint.todo.controller;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import group.tothepoint.todo.model.TodoItem;
import group.tothepoint.todo.service.TodoService;

@RestController("/todo")
public class TodoController {
    private TodoService service;

    public TodoController(final TodoService service) {
        this.service = service;
    }

    @PostMapping()
    public TodoItem addTodo(@RequestBody final String todo) {
        return service.addTodo(todo);
    }

    @GetMapping
    public List<TodoItem> findAll() {
        return service.findAll();
    }
}
